# OpenML dataset: leaf

https://www.openml.org/d/1482

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Pedro F.B. Silva, Andre R.S. Marcal, Rubim M. Almeida da Silva    

**Source**: UCI 

**Please cite**: 'Evaluation of Features for Leaf Discrimination', Pedro F.B. Silva, Andre R.S. Marcal, Rubim M. Almeida da Silva (2013). Springer Lecture Notes in Computer Science, Vol. 7950, 197-204.
 

Abstract: 

This dataset consists in a collection of shape and texture features extracted from digital images of leaf specimens originating from a total of 40 different plant species.

Source:

This dataset was created by Pedro F. B. Silva and Andre R. S. Marcal using leaf specimens collected by Rubim Almeida da Silva at the Faculty of Science, University of Porto, Portugal.


Data Set Information:

For further details on this dataset and/or its attributes, please read the 'ReadMe.pdf' file included and/or consult the Master's Thesis 'Development of a System for Automatic Plant Species Recognition' available at [Web Link].


Attribute Information:

1. Class (Species) 
2. Specimen Number 
3. Eccentricity 
4. Aspect Ratio 
5. Elongation 
6. Solidity 
7. Stochastic Convexity 
8. Isoperimetric Factor 
9. Maximal Indentation Depth 
10. Lobedness 
11. Average Intensity 
12. Average Contrast 
13. Smoothness 
14. Third moment 
15. Uniformity 
16. Entropy

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1482) of an [OpenML dataset](https://www.openml.org/d/1482). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1482/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1482/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1482/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

